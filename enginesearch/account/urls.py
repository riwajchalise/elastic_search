from django.urls import path
from . import views

app_name = 'account'

urlpatterns = [
    path('custreg/', views.custreg, name='custreg'),
    path('products/', views.products, name='products'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('add_product', views.add_product, name='add_products'),
    path('profile/', views.profile, name = 'profile'),

    ]
