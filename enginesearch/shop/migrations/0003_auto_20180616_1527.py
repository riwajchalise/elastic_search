# Generated by Django 2.0.6 on 2018-06-16 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_category_url'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ('rating',), 'verbose_name': 'category', 'verbose_name_plural': 'categories'},
        ),
        migrations.AddField(
            model_name='category',
            name='rating',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='category',
            name='url',
            field=models.CharField(default='www.exampleurl.com', max_length=100),
        ),
    ]
